# vue-draggable-resizable

支持拖拉位置，拖拽放大缩小

## 使用

```js
import VueDraggableResizable from 'mo/vue-draggable-resizable';

components: {
    VueDraggableResizable
}
```

```html
<vue-draggable-resizable
        ref="dragResize"
        :w="VDR.w"
        :h="VDR.h"
        :minw="VDR.minw"
        :minh="VDR.minh"
        :class="[$style.draggable, $style[VDR.positionTemp]]"
        :x="VDR.x"
        :y="VDR.y"
        :parent="VDR.parent"
        :handles="VDR.handles"
        :grid="VDR.grid"
        @dragging="onDragging"
    ></vue-draggable-resizable>
```

## 文档

[文档地址](https://github.com/mauricius/vue-draggable-resizable)
