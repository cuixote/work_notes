import _ from "lodash";
import Vue from "vue";
import {
  showImage,
  forSake,
  setProcessedToUseless,
  getImageTags,
  getImgTypeList,
  getCopyImage,
  getEditImageClassifyInfo,
  saveImageClassifyInfo,
  saveForAudit
} from "src/fetchs/img";

const Message = Vue.prototype.$message;
export const SET_IMAGE_TYPE_LIST = "SET_IMAGE_TYPE_LIST";
export const SET_PROCESSED_LIST = "SET_PROCESSED_LIST";
export const SET_PROCESS_LIST = "SET_PROCESS_LIST";
export const SET_USELESS_LIST = "SET_USELESS_LIST";
export const MOVE_PROCESS_TO_PROCESSED = "MOVE_PROCESS_TO_PROCESSED";
export const MOVE_USELESS_TO_PROCESSED = "MOVE_USELESS_TO_PROCESSED";
export const MOVE_PROCESS_TO_USELESS = "MOVE_PROCESS_TO_USELESS";
export const MOVE_PROCESSED_TO_USELESS = "MOVE_PROCESSED_TO_USELESS";
export const SET_LOADING = "SET_LOADING";
export const SET_EDIT_STATE = "SET_EDIT_STATE";
export const SET_CURRENT_LIST_NAME = "SET_CURRENT_LIST_NAME";
export const SET_CURRENT_IMG = "SET_CURRENT_IMG";
export const SET_CURRENT_IMG_ID = "SET_CURRENT_IMG_ID";
export const SET_CURRENT_IMG_EDITABLE = "SET_CURRENT_IMG_EDITABLE";
export const SET_CURRENT_IMG_EDIT_TYPE = "SET_CURRENT_IMG_EDIT_TYPE";
export const SET_CURRENT_IMG_CLASSIFY_INFO_FORM_DATA =
  "SET_CURRENT_IMG_CLASSIFY_INFO_FORM_DATA";
export const SET_CURRENT_IMG_PROFILE_ID = "SET_CURRENT_IMG_PROFILE_ID";
export const SET_CURRENT_IMG_MEDICAL_HISTORY_FORM_DATA =
  "SET_CURRENT_IMG_MEDICAL_HISTORY_FORM_DATA";
export const CLEAR_IMG_LIB = "CLEAR_IMG_LIB";

const initialState = {
  // 图片分类
  imageTypeList: [],
  // 已处理图片列表
  processedList: [],
  // 待处理图片列表
  processList: [],
  // 无用图片列表
  uselessList: [],
  // 图库全局加载动画
  loading: true,
  // 图库状态： false: view, true: edit
  editState: false,
  // 图库当前列表 processedList processList uselessList
  currentListName: "processList",
  // 图库当前图片
  currentImg: null,
  // 图库当前图片的图库id
  currentImgID: null,
  // 图库当前图片是否可编辑 false: 否 true: 是
  currentImgEditable: false,
  // 图库当前图片的编辑类型 1：仅编辑分类2：录入病历
  currentImgEditType: 1,
  // 图库当前图片编辑类型为1时，需要的初始化数据
  currentImgClassifyInfoFormData: {
    classifyTypeName: "",
    time: ""
  },
  // 图库当前图片的profileID
  currentImgProfileID: "",
  // 图库当前图片编辑类型为2时，需要的初始化数据
  currentImgMedicalHistoryFormData: {}
};

const mutations = {
  // 设置「图片分类」列表
  [SET_IMAGE_TYPE_LIST](state, payload = []) {
    state.imageTypeList = payload;
  },
  // 设置「已处理」图片列表
  [SET_PROCESSED_LIST](state, payload = []) {
    state.processedList = _.cloneDeep(payload);
  },
  // 设置「待处理」图片列表
  [SET_PROCESS_LIST](state, payload = []) {
    state.processList = _.cloneDeep(payload);
  },
  // 设置「无用图片」列表
  [SET_USELESS_LIST](state, payload = []) {
    state.uselessList = _.cloneDeep(payload);
  },
  // 将图片从「待处理」图片列表移动到「已处理」图片列表
  [MOVE_PROCESS_TO_PROCESSED](state, imageKey) {
    const index = state.processList
      .map(item => item.imageKey)
      .indexOf(imageKey);
    const img = state.processList.splice(index, 1);
    state.processedList = [...img, ...state.processedList];
  },
  // 将图片从「无用图片」列表移动到「已处理」图片列表
  [MOVE_USELESS_TO_PROCESSED](state, imageKey) {
    const index = state.uselessList
      .map(item => item.imageKey)
      .indexOf(imageKey);
    const img = state.uselessList.splice(index, 1);
    state.processedList = [...img, ...state.processedList];
  },
  // 将图片从「待处理」图片列表移动到「无用图片」列表
  [MOVE_PROCESS_TO_USELESS](state, imageKey) {
    const index = state.processList
      .map(item => item.imageKey)
      .indexOf(imageKey);
    const img = state.processList.splice(index, 1);
    state.uselessList = [...img, ...state.uselessList];
  },
  // 将图片从「已处理」图片列表移动到「无用图片」列表
  [MOVE_PROCESSED_TO_USELESS](state, imageKey) {
    const index = state.processedList
      .map(item => item.imageKey)
      .indexOf(imageKey);
    const img = state.processedList.splice(index, 1);
    state.uselessList = [...img, ...state.uselessList];
  },
  // 设置图库全局加载动画
  [SET_LOADING](state, payload) {
    state.loading = payload;
  },
  // 设置图库状态
  [SET_EDIT_STATE](state) {
    state.editState = !state.editState;
  },
  // 设置图库当前列表
  [SET_CURRENT_LIST_NAME](state, payload) {
    state.currentListName = payload;
  },
  // 设置图库当前图片
  [SET_CURRENT_IMG](state, payload) {
    state.currentImg = payload;
  },
  // 设置图库当前图片图库id
  [SET_CURRENT_IMG_ID](state, payload) {
    state.currentImgID = payload;
  },
  // 设置图库当前图片是否可编辑
  [SET_CURRENT_IMG_EDITABLE](state, payload) {
    state.currentImgEditable = payload;
  },
  // 设置图库当前图片编辑类型
  [SET_CURRENT_IMG_EDIT_TYPE](state, payload) {
    state.currentImgEditType = payload;
  },
  // 设置图库当前图片编辑类型为1时，表格数据
  [SET_CURRENT_IMG_CLASSIFY_INFO_FORM_DATA](state, payload) {
    state.currentImgClassifyInfoFormData = payload;
  },
  // 设置图库当前图片profileID
  [SET_CURRENT_IMG_PROFILE_ID](state, payload) {
    state.currentImgProfileID = payload;
  },
  // 设置图库当前图片编辑类型为2时，表格数据
  [SET_CURRENT_IMG_MEDICAL_HISTORY_FORM_DATA](state, payload) {
    state.currentImgMedicalHistoryFormData = payload;
  },
  // 清空/初始化 图库数据
  [CLEAR_IMG_LIB](state) {
    const initState = _.cloneDeep(initialState);
    Object.keys(initState).forEach(i => {
      state[i] = initState[i];
    });
  }
};

const actions = {
  // 获取图片分类列表
  async getImagesTypeList({ commit }) {
    try {
      commit(SET_IMAGE_TYPE_LIST, await getImgTypeList());
    } catch (e) {
      Message.error("获取图片分类列表失败");
      return Promise.reject(e);
    }
  },
  // 获取指定患者的图库
  async fetchImgList(context, patientId) {
    try {
      const {
        arrangedImageList = [],
        notArrangedImageList = [],
        uselessImageList = []
      } = await showImage(patientId);
      context.commit(SET_PROCESSED_LIST, arrangedImageList);
      context.commit(SET_PROCESS_LIST, notArrangedImageList);
      context.commit(SET_USELESS_LIST, uselessImageList);
    } catch (e) {
      Message.error("获取患者的图库失败");
      return Promise.reject(e);
    }
  },
  // 「待处理」图片设为「无用图片」
  async signProcessToUseless({ state, commit }, imageKey) {
    try {
      await forSake(imageKey);
      commit(MOVE_PROCESS_TO_USELESS, imageKey);
    } catch (e) {
      Message.error("「待处理」图片设为「无用图片」失败");
      return Promise.reject(e);
    }
  },
  // 「已处理」图片转为「无用图片」
  async signProcessedToUseless({ state, commit }, payload) {
    try {
      const { imageLibraryId, imageKey } = payload;
      await setProcessedToUseless(imageLibraryId);
      commit(MOVE_PROCESSED_TO_USELESS, imageKey);
    } catch (e) {
      Message.error("「已处理」图片转为「无用图片」失败");
      return Promise.reject(e);
    }
  },
  // 获取当前图片信息
  async getCurrentImgInfo(context, payload) {
    try {
      const { patientId, imageKey } = payload;
      const { canEdit, imageLibraryId } = await getImageTags(
        patientId,
        imageKey
      );
      context.commit(SET_CURRENT_IMG_ID, imageLibraryId);
      context.commit(SET_CURRENT_IMG_EDITABLE, !!canEdit);
    } catch (e) {
      Message.error("获取当前图片信息失败");
      return Promise.reject(e);
    }
  },
  // 复制图片
  async getImageCopyById({ state, commit, dispatch }, payload) {
    try {
      const img = await getCopyImage(state.currentImgID);
      commit(SET_CURRENT_IMG, img);
      await dispatch("getCurrentImgInfo", {
        patientId: payload,
        imageKey: img.imageKey
      });
      state.processList = [img, ...state.processList];
      commit(SET_CURRENT_LIST_NAME, "processList");
      Message.success("复制图片成功");
    } catch (e) {
      Message.error("复制图片失败");
      return Promise.reject(e);
    }
  },
  // 获取当前图片编辑信息
  async getEditImageInfo(context, currentImgID) {
    try {
      const {
        editType,
        imageLibraryId,
        classifyInfo,
        profileId
      } = await getEditImageClassifyInfo(currentImgID);
      context.commit(SET_CURRENT_IMG_EDIT_TYPE, editType);
      context.commit(SET_CURRENT_IMG_ID, imageLibraryId);
      context.commit(SET_CURRENT_IMG_PROFILE_ID, profileId);
      classifyInfo &&
        context.commit(SET_CURRENT_IMG_CLASSIFY_INFO_FORM_DATA, classifyInfo);
    } catch (e) {
      Message.error("获取当前图片编辑信息失败");
      return Promise.reject(e);
    }
  },
  // 保存编辑信息 「仅标记图片分类」
  async saveImageClassifyInfo({ state, commit }, payload) {
    try {
      await saveImageClassifyInfo(payload);
      /*
            * 保存成功后，
            * 1. 修改当前图片的信息
            * 2. 需要从当前列表跳转到已处理列表
            * */
      // 1
      state.currentImg.time = payload.date;
      state.currentImg.group = payload.date;
      for (const item of state.imageTypeList) {
        if (item.typeId === payload.classifyType) {
          state.currentImg.imageClassifyName = item.typeName;
          break;
        }
      }
      commit(SET_CURRENT_IMG_PROFILE_ID, null);
      // 2
      if (state.currentListName === "processList") {
        commit(MOVE_PROCESS_TO_PROCESSED, state.currentImg.imageKey);
      } else if (state.currentListName === "uselessList") {
        commit(MOVE_USELESS_TO_PROCESSED, state.currentImg.imageKey);
      }
      commit(SET_CURRENT_LIST_NAME, "processedList");
      commit(SET_EDIT_STATE, false);
    } catch (e) {
      Message.error("「仅标记图片分类」失败");
      return Promise.reject(e);
    }
  },
  // 保存编辑信息 「录入至病历」
  async saveForAudit({ state, commit }, payload) {
    try {
      const { time, imageClassifyName } = await saveForAudit(payload);
      /*
            * 保存成功后，
            * 1. 修改当前图片的信息
            * 2. 需要从当前列表跳转到已处理列表
            * */
      // 1
      state.currentImg.time = time;
      state.currentImg.group = time;
      state.currentImg.imageClassifyName = imageClassifyName;
      state.currentImg.isMedicalRecord = true;
      commit(SET_CURRENT_IMG_EDITABLE, false);
      commit(SET_CURRENT_IMG_PROFILE_ID, null);
      // 2
      if (state.currentListName === "processList") {
        commit(MOVE_PROCESS_TO_PROCESSED, state.currentImg.imageKey);
      } else if (state.currentListName === "uselessList") {
        commit(MOVE_USELESS_TO_PROCESSED, state.currentImg.imageKey);
      }
      commit(SET_CURRENT_LIST_NAME, "processedList");
      commit(SET_EDIT_STATE, false);
    } catch (e) {
      Message.error("「录入至病历」失败");
      return Promise.reject(e);
    }
  }
};

export default {
  name: "gallery",
  namespaced: true,
  state: _.cloneDeep(initialState),
  mutations,
  actions
};
