# 图片查看组件 viewer

## 使用

```html
<ViewerCom ref="viewerCom" :list="totalImgList" :active="activeImg" />
```

```js
import ViewerCom from 'mo/viewer';

components: {ViewerCom}
```

组件对外暴露了三个事件：

1. init 初始化事件
2. change 切换图片事件，参数为要切换图片在图片列表中的index
3. destroy 销毁事件

使用：

```js
this.$refs.viewerCom.$emit('destroy');

this.$refs.viewerCom.$emit('change', 3);
```

## props

- options viewer组件内部配置项，非必传。会覆盖默认配置。默认配置见组件代码。[配置项文档](https://github.com/fengyuanchen/viewerjs)

- list 图片列表，必传

- active 当前图片，必传

- config 此组件整体配置项，非必传
