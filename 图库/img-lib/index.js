import Vue from 'vue';
import store from 'src/store';
import router from 'src/route';
import Modal from './index.vue';

window._imgLib_component_close = () => {
    if (window._imgLib_component) {
        window._imgLib_component.$el.parentNode.removeChild(window._imgLib_component.$el);
        window._imgLib_component.$destroy();
        window._imgLib_component = null;
    }
};
export default function render(args) {
    window._imgLib_component_close();
    window._imgLib_component = new Vue({
        store,
        router,
        render: h =>
            h(Modal, {
                props: args,
            }),
    }).$mount();
    document.body.appendChild(window._imgLib_component.$el);
}
