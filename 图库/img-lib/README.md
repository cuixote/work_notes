# 图库

> 管理某个患者的所有图片。

-   图片默认分为待处理(process)、已处理(processed)、无用图片(useless)。

-   单张图片可以编辑和预览。

-   可上传图片。上传后的图片会进入到待处理。

-   可复制图片。复制后的新图片会进入到待处理。

-   左侧列表支持拖拽。（场景：拖拽到上传组件上传）

## 示例

### 使用

```js
// 引用
import imgLib from 'src/w/img-lib';

// 使用
imgLib({
    current: String, // 当前图片的imageKey。非必传。默认为空。
    patientId: Number, // 患者ID。非必传。默认会从url中获取。
    position: String, // 图库初始显示位置。非必传。默认是left。可选值：left、right。
    uploadTargetId: String, // 放置点元素ID。非必传。默认为空。
});

// 全局变量
window._imgLib_component;

// 关闭
window._imgLib_component.closeImgLib;
```

## 结构概述

### 图库主目录

```bash
├── README.md  // 说明文档
├── components
│   ├── edit-panel.vue  // 图片编辑模块
│   └── tab-list.vue  // 图片列表
├── img-lib.js // 主要逻辑
├── index.js // 构造函数。图库入口
├── index.vue // HTML模版
└── style.less // 样式文件
```

### viewer 组件

图库与此独立组件依赖性很强。数据通过`props`传递。配置项通过`config`配置。

-   组件路径 `src/components/viewer`

### vuex

图库数据管理主要通过 Vuex。所有最底层的数据都在 Vuex 进行管理。

-   初始化数据： `state`

-   同步数据操作：`mutations`

-   异步数据操作：`actions`

-   文件路径 `src/store/module/gallery`

### vue-draggable-resizable

图库的拖拽、缩放。

- 文化路径 `src/components/vue-draggable-resizable`
