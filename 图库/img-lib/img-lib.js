import { mapActions, mapMutations, mapState } from "vuex";
import ViewerCom from "mo/viewer";
import ImageUploadOnly from "src/components/image-upload-only";
import VueDraggableResizable from "mo/vue-draggable-resizable";

import { uploadMutiImageToLib } from "src/fetchs/img";
import storeGallery, {
  CLEAR_IMG_LIB,
  SET_EDIT_STATE,
  SET_LOADING,
  SET_CURRENT_IMG,
  SET_CURRENT_IMG_ID,
  SET_CURRENT_LIST_NAME,
  SET_CURRENT_IMG_PROFILE_ID
} from "src/store/modules/gallery";

import TabList from "./components/tab-list";
import EditPanel from "./components/edit-panel";

export default {
  name: "ImgLib",
  components: {
    VueDraggableResizable,
    ImageUploadOnly,
    ViewerCom,
    TabList,
    EditPanel
  },
  props: {
    current: {
      required: false,
      type: String,
      default: ""
    },
    patientId: {
      required: false,
      type: Number,
      default: () => {
        const { patientId } = YB.getPageUrlSearchParam();
        return patientId ? +patientId : "";
      }
    },
    position: {
      required: false,
      type: String,
      default: "left"
    },
    uploadTargetId: {
      required: false,
      type: String,
      default: ""
    }
  },
  data() {
    return {
      // 配置vue-draggable-resizable
      VDR: {
        w: window.innerWidth * 0.64,
        h: window.innerHeight,
        minw: 600,
        minh: window.innerHeight,
        x: 0,
        y: 0,
        z: 2001,
        init: true,
        parent: true,
        handles: ["ml", "mr"],
        grid: [30, 30],
        dragHandle: ".drag",
        dragCancel: ".drag-cancel",
        position: this.position
      },
      // 显示上传图片弹窗
      showUploadImgDialog: false,
      // 上传图片表单
      UploadImgForm: {
        doctorVisit: 0,
        imgList: []
      }
    };
  },
  computed: {
    ...mapState(storeGallery.name, [
      "processedList",
      "processList",
      "uselessList",
      "loading",
      "editState",
      "currentListName",
      "currentImg",
      "currentImgEditable"
    ]),
    // 根据当前列表名称获取当前列表
    currentList() {
      return this[this.currentListName];
    },
    // 配置Viewer组件。非组件内部配置，只是组件外的一些配件（EditBar、OCRBar）的显示隐藏
    viewComConfig() {
      let imgRightBottomText = "";
      if (this.currentListName === "processList") {
        imgRightBottomText = "上传待处理图片";
      }
      if (this.currentListName === "uselessList") {
        imgRightBottomText = "上传无用图片";
      }
      const showEditBtn = !this.editState && this.currentImgEditable;
      const showSignUselessBtn =
        !this.editState &&
        (this.currentListName === "processList" ||
          this.currentListName === "processedList");
      return {
        showEditBtn,
        showSignUselessBtn,
        // 图片编辑有两个按钮：标记无用（showSignUselessBtn）和编辑图片（showEditBtn）。至少有一个显示才显示bar。
        showEditBar: showEditBtn || showSignUselessBtn,
        showOCRBar: this.editState,
        showOCRResult: this.editState,
        imgRightBottomText,
        showCopyBtn: !this.editState
      };
    }
  },
  mounted() {
    // 初始化组件
    this.init();
  },
  methods: {
    ...mapActions(storeGallery.name, [
      "fetchImgList",
      "getCurrentImgInfo",
      "getImagesTypeList"
    ]),
    ...mapMutations(storeGallery.name, {
      setLoading: SET_LOADING,
      setEditState: SET_EDIT_STATE,
      clearImgLib: CLEAR_IMG_LIB,
      setCurrentImg: SET_CURRENT_IMG,
      setCurrentImgId: SET_CURRENT_IMG_ID,
      setCurrentListName: SET_CURRENT_LIST_NAME,
      setCurrentImgProfileId: SET_CURRENT_IMG_PROFILE_ID
    }),
    // 设置图库z-index
    setZIndex(elem = "el-dialog__wrapper") {
      const elems = document.getElementsByClassName(elem);
      let highest = this.VDR.z;
      for (let i = 0; i < elems.length; i++) {
        const zindex = document.defaultView
          .getComputedStyle(elems[i], null)
          .getPropertyValue("z-index");
        if (zindex > highest && zindex !== "auto") {
          highest = zindex;
        }
      }
      this.VDR.z = +highest;
    },
    // 批量上传图片
    async handleUploadImg() {
      try {
        this.setLoading(true);
        const { patientId } = this;
        const { doctorVisit, imgList } = this.UploadImgForm;
        await uploadMutiImageToLib({
          patientId,
          doctorVisit,
          imageKeyList: imgList.map(item => item.imageKey)
        });
        this.$message.success("上传成功");
        this.showUploadImgDialog = false;
        this.UploadImgForm = {
          doctorVisit: 1,
          imgList: []
        };
        // 刷新列表
        this.refreshList();
      } catch (e) {
        console.log(e);
      }
      this.setLoading(false);
    },
    // 刷新列表
    async refreshList() {
      // 清空当前数据
      this.clearImgLib();
      const { patientId } = this;
      this.setLoading(true);
      // 取图片分类列表，存入Vuex
      await this.getImagesTypeList();
      // 获取图片列表
      await this.fetchImgList(patientId);
      // 取当前列表第一张图片
      const currentImgTemp = this[this.currentListName][0] || null;
      // 如果存在，取图片信息
      if (currentImgTemp) {
        await this.getCurrentImgInfo({
          patientId,
          imageKey: currentImgTemp.imageKey
        });
        this.setCurrentImg(currentImgTemp);
      } else {
        this.setCurrentImg(currentImgTemp);
        this.setCurrentImgId(null);
      }
      // 初始化Viewer组件
      this.$refs.viewerCom.$emit("init");
      // 关闭loading
      this.setLoading(false);
    },
    // 从编辑返回到浏览
    handleReturn() {
      this.setEditState();
      this.setCurrentImgProfileId(null);
    },
    // 销毁组件
    handleClose() {
      this.$refs.viewerCom.$emit("destroy");
      this.clearImgLib();
      window._imgLib_component_close();
    },
    // 初始化组件
    async init() {
      try {
        this.setZIndex();
        const { patientId } = this;
        // 取图片分类列表，存入Vuex
        await this.getImagesTypeList();
        // 获取图片列表
        await this.fetchImgList(patientId);
        // 是否有选中某张图片
        if (this.current) {
          // 获取当前图片信息
          await this.getCurrentImgInfo({
            patientId,
            imageKey: this.current
          });
          // 找到图片所在列表，及列表中的位置，并切换列表，选中
          this.whereAmI(this.current);
        } else {
          // 去当前列表第一张图片
          const currentImgTemp = this[this.currentListName][0] || null;
          // 如果存在，取图片信息
          if (currentImgTemp) {
            await this.getCurrentImgInfo({
              patientId,
              imageKey: currentImgTemp.imageKey
            });
            this.setCurrentImg(currentImgTemp);
          } else {
            this.setCurrentImg(currentImgTemp);
            this.setCurrentImgId(null);
          }
        }
        // 初始化Viewer组件
        this.$refs.viewerCom.$emit("init");
      } catch (e) {
        console.log(e);
      }
      // 关闭loading
      this.setLoading(false);
    },

    // 在三个列表中找到传入的ImgKey的位置
    whereAmI() {
      const circulation = (list, name) => {
        for (let i = 0; i < list.length; i++) {
          if (list[i].imageKey === this.current) {
            this.setCurrentImg(list[i]);
            this.setCurrentListName(name);
            break;
          }
        }
      };
      circulation(this.processedList, "processedList");
      this.currentImg || circulation(this.processList, "processList");
      this.currentImg || circulation(this.uselessList, "uselessList");
    },

    // 预览指定图片
    handleView(index) {
      this.$refs.viewerCom.$emit("change", index);
    },

    // 移动图库
    onDragging(left, top) {
      if (this.VDR.position === "right") {
        this.VDR.x = window.innerWidth - this.$el.offsetWidth;
        this.VDR.position = "";
      } else {
        this.VDR.x = left;
      }
      this.VDR.y = top;
    }
  }
};
