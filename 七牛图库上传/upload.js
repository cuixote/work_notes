import Request from 'src/utils/request'

/**
 * 获取token接口
 * @param bucket
 */
const getUploadTokenApi = function (bucket) {
  return Request({
    url: `/qiniu/${bucket}/token`
  })
}

class UploadError extends Error {
  constructor (message) {
    super()
    this.message = message
    this.name = this.constructor.name
  }
}

class Upload {
  constructor (options = {}) {
    if (options.config && typeof options.config !== 'object') {
      throw new UploadError('options.config类型必须为Object')
    }
    this.config = Object.assign({
      // 可在此处进行默认全局配置
    }, options.config)
    if (options.putExtra && typeof options.putExtra !== 'object') {
      throw new UploadError('options.config类型必须为Object')
    }
    this.putExtra = Object.assign({
      // 可在此处进行默认全局配置
    }, options.putExtra)
    if (options.compressConfig && typeof options.compressConfig !== 'object') {
      throw new UploadError('options.compressConfig类型必须为Object')
    }
    this.compressConfig = Object.assign({
      // 可在此处进行默认全局配置
      compress: false
    }, options.compressConfig)
  }

  /**
   * 生成唯一的文件名
   * @param type 文件类型
   * @returns {string} 文件名
   */
  static generateFileName (type) {
    const suffix = type.replace('image/', '').toLowerCase()
    const fileName = Date.now().toString(35) + Math.random().toString(35).slice(2)
    return `${process.env.VUE_APP_ENV}/${process.env.VUE_APP_BASE_NAME}/${fileName}.${suffix}`
  }

  /**
   * 实例方法： 生成唯一的文件名
   * @param type 文件类型
   * @returns {string} 文件名
   */
  generateFileName (type) {
    return Upload.generateFileName(type)
  }

  /**
   * 校验文件名是否合法
   * @param file 文件
   * @param name 文件名
   * @returns {boolean}
   */
  parseFileName (file, name) {
    return new RegExp(`${process.env.VUE_APP_ENV}/${process.env.VUE_APP_BASE_NAME}/[a-z0-9]+\\.${file.type.replace('image/', '').toLowerCase()}`).test(name)
  }

  /**
   * 修改上传方法为promise形式
   * @param observable
   * @param next
   * @returns {Promise<any>}
   */
  promisify (observable, next) {
    return new Promise((resolve, reject) => {
      const complete = (resolve) => {
        return res => {
          resolve(res.key)
        }
      }
      const error = (reject) => {
        return err => {
          reject(err)
        }
      }
      const progress = (next) => {
        return data => {
          let percent = data.total.percent
          next(Number(Number.prototype.toFixed.call(percent, 2)))
        }
      }
      const subscription = observable.subscribe(progress(next), error(reject), complete(resolve))
      this.cancel = () => {
        subscription.unsubscribe()
      }
    })
  }
  /**
   * 获取token
   * @returns {Promise<*>}
   */
  static async getUploadToken () {
    try {
      // token 的有效期默认为一个小时
      // 为了避免多图上传时，在短时间内重复获取token
      // 将token暂存在内存中
      if (window.QINIU_UPLOAD_FILE_TOKEN) {
        const { time, token } = window.QINIU_UPLOAD_FILE_TOKEN
        if (Date.now() - time > 59 * 60 * 1000) {
          const newToken = await getUploadTokenApi(process.env.VUE_APP_UPLOAD_BUCKET)
          window.QINIU_UPLOAD_FILE_TOKEN = {
            time: Date.now(),
            token: newToken
          }
          return newToken
        } else {
          return token
        }
      } else {
        const newToken = await getUploadTokenApi(process.env.VUE_APP_UPLOAD_BUCKET)
        window.QINIU_UPLOAD_FILE_TOKEN = {
          time: Date.now(),
          token: newToken
        }
        return newToken
      }
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * 实例方法
   * @returns {Promise<*>}
   */
  getUploadToken () {
    return Upload.getUploadToken()
  }

  /**
   * 上传方法
   * @param file 文件
   * @param next
   * @param name 文件名
   * @param options 配置项
   * @returns {Promise<*>}
   */
  async post (file, next = () => {}, name = '', options = {}) {
    if (typeof file !== 'object') {
      throw new UploadError('参数file必传，且类型为file对象')
    }
    // 如果文件名已经确定
    if (name) {
      if (!this.parseFileName(file, name)) {
        throw new UploadError('文件命名格式有误！请使用`Upload.generateFileName`方法生成正确的文件名！')
      }
    }
    try {
      this.token = await this.getUploadToken()
      // 如果开启压缩
      if (this.compressConfig.compress || options.compress) {
        file = await this.compress(file, options)
      }
      const observable = window.qiniu.upload(file, name || this.generateFileName(file.type), this.token, this.putExtra, this.config)
      return await this.promisify(observable, next)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * 压缩图片方法
   * @param file
   * @param options [Object] {
   *   quality: Number 图片压缩质量，在图片格式为 image/jpeg 或 image/webp 的情况下生效，其他格式不会生效，
   *   可以从 0 到 1 的区间内选择图片的质量。默认值 0.92
   *   maxWidth: Number 压缩图片的最大宽度值
   *   maxHeight: Number 压缩图片的最大高度值 （注意：当 maxWidth 和 maxHeight 都不设置时，则采用原图尺寸大小）
   *   noCompressIfLarger: Boolean 为 true 时如果发现压缩后图片大小比原来还大，则返回源图片（即输出的 dist 直接返回了输入的 file）；
   *   默认 false，即保证图片尺寸符合要求，但不保证压缩后的图片体积一定变小
   * }
   * @returns {Promise<*>} {
   *   dist: [Blob Object]
   *   width: Number
   *   height: Number
   * }
   */
  async compress (file, options = {}) {
    options = Object.assign(this.compressConfig, options)
    // 如果文件类型是'image/jpeg', 'image/webp'，或者maxWidth，maxHeight参数传了其中一个，则进行图片压缩
    if (['image/jpeg', 'image/webp'].includes(file.type) || options.maxWidth !== undefined || options.maxHeight !== undefined) {
      try {
        const { dist } = await window.qiniu.compressImage(file, options)
        return dist
      } catch (e) {
        return Promise.reject(e)
      }
    } else {
      return file
    }
  }
  // 取消上传方法
  cancel () {
  }
}

export default Upload
