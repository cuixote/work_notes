/**
 * 药物实际使用记录表 保存
 * @param patientId
 * @param data
 * @returns {*}
 */
export function saveDrugActualDoseFormApi(patientId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/task/saveSingleDrugActualDoseForm`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 药物治疗信息表 保存
 * @param patientId
 * @param data
 * @returns {*}
 */
export function saveDrugTreatFormApi(patientId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/task/saveSingleDrugTreatForm`,
        type: 'post',
        method: 'raw',
        data,
    });
}
