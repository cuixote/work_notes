import { getPatientDrugListByPatientIdApi } from 'src/fetchs/profile';
import RelaxedDatePicker from 'mo/relaxed-date-picker';

export default {
    components: {
        RelaxedDatePicker,
    },
    data() {
        return {
            // 表单是否可编辑
            ableForm: true,
        };
    },
    created() {
        // 如果data里有patientDrugList，说明需要患者个人药物库，就去请求
        this.patientDrugList && this.getPatientDrugsList();
    },
    // 任务的暂存方法
    inject: ['handleSave'],
    methods: {
        // 获取指定患者药物库
        async getPatientDrugsList() {
            try {
                const { patientId } = YB.getPageUrlSearchParam();
                this.patientDrugList = await getPatientDrugListByPatientIdApi(patientId);
            } catch (e) {
                this.$message.error('获取患者药物失败');
            }
        },
        // 默认初始化表单数据，如果需要对数据进行处理，可覆盖此方法
        initForm(form) {
            this.form = Object.assign({}, this.form, form);
        },
        // 校验数字
        handleValidateNumber(rule, value, callback, type) {
            if (Number.isNaN(Number(value))) {
                callback(new Error('请输入数字'));
                return;
            }
            if (type === 'float') {
                callback();
            } else if (type === 'integer') {
                if (Number.isInteger(Number(value))) {
                    callback();
                } else {
                    callback(new Error('请输入整数'));
                }
            }
        },
        // 校验开始日期和结束日期
        handleDateValidate(start, end, callback) {
            const startArr = start.split('-');
            const endArr = end.split('-');
            if (+startArr[0] > +endArr[0]) {
                callback(new Error('开始日期不能大于结束日期'));
            } else if (+startArr[0] === +endArr[0]) {
                if (+startArr[1] > +endArr[1]) {
                    callback(new Error('开始日期不能大于结束日期'));
                } else if (+startArr[1] === +endArr[1]) {
                    if (!startArr[2] || !endArr[2]) {
                        callback();
                    } else if (+startArr[2] > +endArr[2]) {
                        callback(new Error('开始日期不能大于结束日期'));
                    } else {
                        callback();
                    }
                } else {
                    callback();
                }
            } else {
                callback();
            }
        },
        // 对日期进行处理
        parseFormatDate(start, end, hasEnd) {
            if (hasEnd === undefined || hasEnd) {
                const startArr = start.split('-');
                const endArr = end.split('-');
                // 如果开始日期或者截止日期为空，直接原样返回
                if (!startArr[0] || !endArr[0]) {
                    return {
                        start,
                        end,
                    };
                }
                // 如果两个日期的「日」都为空，则都设置为1
                if (!startArr[2] && !endArr[2]) {
                    startArr[2] = '01';
                    endArr[2] = '01';
                    return {
                        start: startArr.join('-'),
                        end: endArr.join('-'),
                    };
                }
                // 如果开始日期的「日」为空
                if (!startArr[2]) {
                    // 如果年月相等，则令日也相等
                    if (startArr[0] === endArr[0] && startArr[1] === endArr[1]) {
                        startArr[2] = endArr[2];
                    }
                    return {
                        start: startArr.join('-'),
                        end: endArr.join('-'),
                    };
                }
                // 如果结束日期的「日」为空
                if (!endArr[2]) {
                    // 如果年月相等，则令日也相等
                    if (startArr[0] === endArr[0] && startArr[1] === endArr[1]) {
                        endArr[2] = startArr[2];
                    }
                    return {
                        start: startArr.join('-'),
                        end: endArr.join('-'),
                    };
                }
                // 如果年月日都有值，直接返回
                return {
                    start,
                    end,
                };
            }
            // 如果未结束治疗，结束治疗时间一律传空
            return {
                start,
                end: '',
            };
        },
    },
};
