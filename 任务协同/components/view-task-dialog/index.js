import Vue from 'vue';
import store from 'src/store';
import router from 'src/route';
import Modal from './dialog.vue';

window._viewTask_component_close = () => {
    if (window._viewTask_component) {
        window._viewTask_component.$el.parentNode.removeChild(window._viewTask_component.$el);
        window._viewTask_component.$destroy();
        window._viewTask_component = null;
    }
};
export default function render(args) {
    window._viewTask_component_close();
    window._viewTask_component = new Vue({
        store,
        router,
        render: h =>
            h(Modal, {
                props: args,
            }),
    }).$mount();
    document.body.appendChild(window._viewTask_component.$el);
}
