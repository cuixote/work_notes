## 查看任务弹窗

查看任务的详情。如果是患者任务，中间部分使用`<iframe>`展示患者端页面。

目前只在任务列表有一个入口。

### 使用说明

```
import ViewTask from 'src/w/view-task-dialog';


ViewTask({
        // 任务类型ID
        taskTypeId: item.taskType,
        // 任务ID
        ruleTaskId: item.ruleTaskId,
        // 是否可操作（编辑，取消，完成）
        isOperable: this.isOperable,
        // 任务流程ID
        taskProcessInstanceId: item.taskProcessInstanceId,
        // 任务状态
        taskStatus: item.bizStatus,
        // 任务标题
        title: item.title && item.taskName ? `${item.title}-${item.taskName}` : '查看患者任务',
    });

```
