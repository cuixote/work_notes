/**
 * 完成任务
 * @param patientId
 * @param taskId
 * @param data
 * @returns {*}
 */
export function finishTaskApi(patientId, taskId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskId}/task/finish`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 获取任务详情
 * @param patientId
 * @param taskId
 * @param id
 * @returns {*}
 */
export function getTaskDetailApi(patientId, taskId, id) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskId}/task/detail/${id}`,
        type: 'get',
    });
}
