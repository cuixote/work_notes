import { mapActions } from 'vuex';
import storeTask from 'src/store/modules/task.js';
import Layout from 'mo/layouts/help-patient-finish-task';
import PagePreview from './components/page-preview';
import BtnBar from './components/btn-bar';
import { finishTaskApi, getTaskDetailApi } from './fetch';

export default {
    name: 'HelpPatientFinishTaskMixin',
    components: {
        Layout,
        PagePreview,
        BtnBar,
    },
    props: {
        // 任务ID
        id: Number,
        // 任务类型
        type: Number,
        // 任务流程ID
        taskProcessInstanceId: Number,
    },
    methods: {
        // 用于更新任务列表
        ...mapActions(storeTask.name, ['getPatientTaskList']),
        // 获取任务详情。某些任务可能在患者端已经有一些操作，产生了数据，所以要先获取已存储的数据。
        async getTaskDetail() {
            try {
                const { patientId } = YB.getPageUrlSearchParam();
                return await getTaskDetailApi(patientId, this.type, this.id);
            } catch (e) {
                this.$message.error('获取任务详情失败');
                console.log(e);
                return {};
            }
        },
        // 关闭弹窗
        handleClose() {
            this.$refs.layout.$emit('close', () => {
                this.$destroy();
            });
        },
        // 校验表单
        handleValidate() {
            return this.$refs.form.validate();
        },
        // 处理要提交的数据
        handleFormatPostData() {
            return {
                ruleTaskId: this.id,
                taskProcessInstanceId: this.taskProcessInstanceId,
                completeTaskData: {
                    ...this.handleFormatFormData(),
                },
            };
        },
        // 处理用户表单的数据。默认，可覆盖。
        handleFormatFormData() {
            return this.form;
        },
        // 完成任务
        handleSubmit() {
            this.$confirm('提交后内容不可修改，是否确认提交?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            })
                .then(async () => {
                    try {
                        await this.handleValidate();
                        try {
                            const { patientId } = YB.getPageUrlSearchParam();
                            await finishTaskApi(patientId, this.type, this.handleFormatPostData());
                            this.$message.success('完成任务成功');
                            await this.getPatientTaskList({ patientId: +patientId });
                            this.$refs.layout.$emit('close');
                        } catch (e) {
                            console.log(e);
                            this.$message.error('完成任务失败');
                        }
                    } catch (e) {
                        console.log(e);
                    }
                })
                .catch(e => e);
        },
    },
};
