## 帮患者完成任务

这是一个弹窗。左侧展示任务在患者端的预览，右侧是操作区，填写内容帮助患者完成当前任务。

并不是每个任务都可以帮助患者完成，这在查看任务页面已经做好判断。
如果需要添加新的帮患者完成任务类型，直接在目录里添加新的文件夹即可。

目前只在查看任务弹窗有入口。

### 使用说明


```js
import helpFinishTask from 'src/w/help-finish-task';

helpFinishTask({
   // 任务流程ID
    taskProcessInstanceId,
    // 任务ID
    id: ruleTaskId,
    // 任务类型ID
    type: taskTypeId,
});
```

- 3个参数均为必传。


### 目录结构

- `mixin`: 文件夹包含了所有任务的公共部分，组件、函数、接口等。
因为这些都有一定的规则，所以关键的业务逻辑都在此。
- `patient-task-x`: 每个任务都对应一个单独的文件夹，文件夹名称中包含该任务的任务类型ID--`taskTypeId`-- 此为唯一性标识。

### 创建新的任务

关键业务逻辑代码都在`mixin`中，所以我们发现创建一个新的任务十分简单。

使用了公共的布局组件，文件位置在`src/components/layouts/help-patient-finish-task.vue`。
布局组件有几个配置项，可在文件中查看。

`slot`一般包括三个固定的组件。预览，表单，按钮组。

按钮组复制粘贴就行。

预览需要传参，如果不需要预览，可在布局组件上控制。

唯一有点操作的也就是表单了。

- `data`的`form`是表单的基本数据模版。这个是必须的。`form`这个字段名不要更改。
- 默认上传的数据就是`form`。如果数据上传前需要处理，可以在`methods`添加`handleFormatFormData`方法覆盖`mixin`中的方法。
- 如果表单需要校验，就添加校验规则，表单提交之前会调用校验。所以务必保证`ref
值为`form`。
- 如果需要使用已存储的表单数据，可以在生命周期中调用`getTaskDetail`方法。

基本示例如下：

```vue
<template lang="html">
    <Layout ref="layout" :width="width">
        <PagePreview slot="body-left" :id="id" :type="type"/>
        <el-form slot="body-right" ref="form" :model="form" :rules="rules" label-position="top">
            ...
        </el-form>
        <BtnBar slot="body-right-footer" @close="handleClose" @submit="handleSubmit"/>
    </Layout>
</template>
<script>
import Mixin from '../mixin/';

export default {
    name: 'PatientTaskX',
    mixins: [Mixin],
    data() {
        return {
            width: '600px',
            form: {
                ...
            },
            rules: {},
        };
    },
    async created() {
        this.form = await this.getTaskDetail();
    },
    methods: {
        handleFormatFormData() {
            return {
                ...this.form,
                ...
            };
        },
    },
};
</script>

```
