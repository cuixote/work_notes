import Vue from 'vue';
import store from 'src/store';
import router from 'src/route';

const Dialog = (args = {}) => {
    // eslint-disable-next-line
    const Model = require(`./patient-task-${args.type}`).default;
    const Wrap = Vue.extend({
        store,
        router,
        render: h =>
            h(Model, {
                props: args,
            }),
    });
    const _component = new Wrap().$mount();
    document.body.appendChild(_component.$el);
};

export default Dialog;
