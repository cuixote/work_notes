/**
 * 获取患者在进行中的服务列表
 */
export function getCommodityListApi(patientId) {
    return YB.Ajax({
        url: `/psc/commodity/${patientId}/commodity`,
        type: 'get',
        data: {
            patientId,
        },
    });
}

/**
 * 创建任务
 */
export function createTaskApi(patientId, taskTypeId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskTypeId}/task/create`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 修改任务
 */
export function modifyTaskApi(patientId, taskTypeId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskTypeId}/task/modify`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 获取任务详情
 */
export function getTaskDetailApi(patientId, taskTypeId, taskId) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskTypeId}/task/detail/${taskId}`,
        type: 'get',
    });
}

/**
 * 咨询医生任务新增时的医生列表和类目列表
 */
export function getDoctorAndItemList(patientId) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/20001/task/initTemplate`,
        type: 'post',
        data: {
            patientId,
        },
    });
}

/**
 * 注意事项任务中的患教文章列表
 */
export function getDoctorArticleList(patientId, title = '') {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/20002/task/doctorArticleList`,
        type: 'get',
        data: {
            title,
        },
    });
}

/**
 * 完善就诊资料任务-获取默认就诊资料类型
 */
export function findRuleTaskImageTypeLibraryList() {
    return YB.Ajax({
        url: `/psc/patient/20003/task/findRuleTaskImageTypeLibraryList`,
        type: 'get',
    });
}
