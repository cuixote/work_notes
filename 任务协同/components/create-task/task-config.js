const CDN_URL = 'https://fe.120yibao.com/follow-up-center/images/';

export const patientTask = [
    {
        id: 20001,
        name: '咨询医生',
        preview: `${CDN_URL}preview-task-20001.png`,
        initFormData: {
            // 告知患者重要性
            importantMsg: '',
            // 病情描述
            patientCondition: '',
            // 患者问题
            questions: [{}],
            // 图片资料
            images: [],
            // 随访问诊补充
            inquiry: '',
            // 患者的医生团队
            myDoctorList: [],
            // 添加的医生
            otherDoctorList: [],
            // 科室列表
            itemList: [],
            // 科室ID
            itemId: undefined,
            // 已选择的患者医生。不必提交。
            myChoseList: [],
        },
    },
    {
        id: 20002,
        name: '查看注意事项',
        initFormData: {
            // 编辑内容的方式。 1 自主编辑 2 从患教文章中选择
            bizType: 1,
            // 患教文章ID
            articleId: undefined,
            // 文章标题
            title: '',
            // 自主编辑文章内容
            info: '',
            // 自主编辑文章内容 富文本 图片
            suggestContentImagekeys: [],
        },
    },
    {
        id: 20003,
        name: '完善就诊资料',
        preview: `${CDN_URL}preview-task-20003.png`,
        initFormData: {
            // 需上传的资料类型
            imageTypeList: [],
            // 告知患者重要性
            importantMsg: '',
        },
    },
    {
        id: 20004,
        name: '到院就诊',
        preview: `${CDN_URL}preview-task-20004.png`,
        initFormData: {
            // 告知患者重要性
            importantMsg: '',
            // 建议就诊时间
            suggestVisitTime: Date.now(),
            // 要咨询的问题
            questions: [
                {
                    id: null,
                    name: null,
                },
            ],
            // 图片
            images: [],
            // 就诊注意事项
            tip: '',
            // 就诊医生ID
            doctorUserId: undefined,
            // 就诊医生名称
            doctorUserName: '',
            // 就诊机构院区Id
            hospitalDistrictId: undefined,
            // 就诊医生院区名称
            hospitalDistrictName: '',
        },
    },
    {
        id: 20005,
        name: '用药任务',
        initFormData: {
            // 患教文章Id
            articleId: undefined,
            // 患教文章标题
            articleName: '',
            // 药品库药物名称Id
            drugId: undefined,
            // 药物名称
            drugName: '',
            // 药物对应的
            treatId: undefined,
            // 医嘱用药记录
            treatDoctorAdviceDoseId: undefined,
            // 医嘱用药开始时间
            treatDrugStartDate: '',
        },
    },
    {
        id: 20006,
        name: '评估任务',
        initFormData: {
            // 评估表单Id
            formTemplateId: undefined,
            // 评估表单名称
            formTemplateName: '',
        },
    },
    {
        id: 20007,
        name: '汇报病情',
        initFormData: {},
    },
];
