## 创建/修改任务说明

任务分为两类：患者任务和随访任务。

随访任务只有一个随访小结。即10009，所以，`follower-task`即随访任务的全部。

### 目录结构

#### 根目录

按照任务分类，分成两个入口：

患者任务 -> `patient-task.vue`

随访任务 -> `follower-task.vue`

任务配置文件 -> `task-config.js`: 包含任务的ID，名称，预览图，初始化表单数据等

#### component

存放每个任务对应的动态表单部分

#### mixin

`task-common` -> 患者任务与随访任务的Mixin

### 使用说明

```
import CreateTask from 'src/w/create-task'


Create({
    // 任务类型ID。编辑时候有
    taskTypeId,
    // 任务ID。编辑时候有
    ruleTaskId,
    // 服务ID。确认服务可传
    commodityId,
})
```

### 新增任务

需要改动3个文件，新增1个文件

- 在 `./task-config.js`新增任务的具体描述。包括`id`、`name`、`preview`、`initFormData`等。
- 新增 `./component/task-form-XXX`文件。内容为动态表单数据。
- 在 `./component/index.js`引入新增的文件
- 在此文档的任务对照表中加入新增的任务




### 任务对照表

任务名称 | 对应命名 | 任务类型
--- |--- | --- 
咨询医生 | 20001 | 患者任务
查看注意事项 | 20002 | 患者任务
完善就诊资料 | 20003 | 患者任务
到院就诊 | 20004 | 患者任务
用药任务 | 20005 | 患者任务
评估任务 | 20006 | 患者任务
汇报病情 | 20007 | 患者任务
随访小结 | 10009 | 随访任务

### 任务配置表 -> `task-config.js`

名称 | 字段 | 类型 | 必填 | 说明
--- | --- |--- | --- | --- 
任务ID | id | `Number` | 是 | 
任务名称 | name | `String` | 是 | 
预览图 | preview | `String` | 否 | 没有则没有该字段，该字段格式为url
初始数据 | initFormData | `Object` | 是 | 对应表单的初始化数据


