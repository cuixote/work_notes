import Vue from 'vue';
import store from 'src/store';
import router from 'src/route';
import Modal from './index.vue';

window._createTask_component_close = () => {
    if (window._createTask_component) {
        window._createTask_component.$el.parentNode.removeChild(window._createTask_component.$el);
        window._createTask_component.$destroy();
        window._createTask_component = null;
    }
};
export default function render(args) {
    window._createTask_component_close();
    window._createTask_component = new Vue({
        store,
        router,
        render: h =>
            h(Modal, {
                props: args,
            }),
    }).$mount();
    document.body.appendChild(window._createTask_component.$el);
}
