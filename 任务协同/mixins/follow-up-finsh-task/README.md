## 任务协同

> [产品文档](https://shimo.im/docs/a6Mi1BFd1pg2Uc5i/)



#### 命名解释

完成任务目录命名 `follow-up-task-10001`

#### 任务状态

1. state 1。 未开始
2. state 2。 进行中
3. state 3。 已完成
4. state 4。 已取消
5. state 5。 超时未完成

#### 随访顾问任务页面对照表

`src/p/follow-up-task-10001`

页面名称 | 对应命名 | 备注 
--- |--- |---
问诊评估-核对用药目的| follow-up-task-10001 |
问诊评估-核对历史用药| follow-up-task-10002 |
问诊评估-排查其他健康情况| follow-up-task-10003 |
问诊评估-调药30天用药评估及指导| follow-up-task-10004 |
就诊指导-复查提醒| follow-up-task-10005 |
就诊指导-问诊指导| follow-up-task-10006 |
医嘱解释| follow-up-task-10007 |
病历结构化| follow-up-task-10008 |
待随访| follow-up-task-10009 |


#### 帮患者完成任务对照表

`src/w/help-finish-task/patient-task-20001`

页面名称 | 对应命名 | 备注 
--- |--- |---
咨询医生| patient-task-20001 |
完善病情资料| patient-task-20003 |
到院就诊| patient-task-20004 |

#### 家庭顾问任务问诊表单对照表

`src/components/task-dynamic-form/forms/form-001`

表格名称 | 对应命名 | 备注
--- |--- | --- 
药物治疗信息表| form-001 |  
药物实际使用过程规范评估表| form-002 |
就诊信息记录表| form-003 |
症状评估表| form-004 |
疾病信息记录表| form-005 |
药物实际使用记录表| form-006 |

