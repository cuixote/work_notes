/**
 * 获取任务操作日志
 * @param ruleTaskId 任务ID
 * @returns {*}
 */
export function getTaskLogListApi(ruleTaskId) {
    return YB.Ajax({
        url: `/psc/task/${ruleTaskId}/log`,
        type: 'get',
    });
}

/**
 * 完成任务
 * @param patientId
 * @param taskId
 * @param data
 * @returns {*}
 */
export function finishTaskApi(patientId, taskId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskId}/task/finish`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 获取任务详情
 * @param patientId
 * @param id
 * @param taskId 任务ID
 * @returns {*}
 */
export function getTaskDetailApi(patientId, id, taskId) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${id}/task/detail/${taskId}`,
        type: 'get',
    });
}

/**
 * 取消任务
 * @param patientId
 * @param data
 * @returns {*}
 */
export function cancelTaskApi(patientId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/task/cancel`,
        type: 'post',
        method: 'raw',
        data,
    });
}

/**
 * 暂存任务
 * @param patientId
 * @param taskId
 * @param data
 * @returns {*}
 */
export function updateTaskApi(patientId, taskId, data) {
    return YB.Ajax({
        url: `/psc/patient/${patientId}/${taskId}/task/update`,
        type: 'post',
        method: 'raw',
        data,
    });
}
