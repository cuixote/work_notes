(function(global) {
    var createScript = function (src) {
        var scriptList = Array.prototype.slice.call(document.getElementsByTagName('script'))
        var scriptSrcList = []
        for (var i = 0; i < scriptList.length; i++) {
            scriptSrcList.push(scriptList[i].src)
        }
        var isLoaded = false
        for (var j = 0; j < scriptSrcList.length; j++ ) {
            if(src === scriptSrcList[j]) {
                isLoaded = true
            }
        }
        if(!isLoaded) {
            var script = document.createElement('script')
            script.src = src
            document.body.append(script)
        }

    }
    function require (path) {
        createScript(path)
    }

    global.define = function (fn) {
        var module = {
            exports: null
        }
        fn.call(null, require, module)
    }
    global.MODULE_LIST = [] 

})(window)